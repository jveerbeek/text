---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

(getting_started)=


# Stylo
Now in the previous chapters, we tried to analyze our texts on corpus level: we wanted to know what patterns occured within our corpus; in what context `men` is discussed, what words frequently co-occur with `woman` and `man`; how positive reviews differ from negative reviews; how texts written by men differ from written by women. We didn't really care about the *individual* texts in our corpus -- whether they differed very much, whether they are similar to each other.

In this chapter, we'll utilize Stylo to find *relationships* between texts. What texts are similar to each other? What clusters can be found in our corpus? To answer these questions, one of the most important things we need is some idea of how to (computationally) measure distances between texts. Because this is a topic which -- again -- could deserve a manual of its own, I will not go into detail on how these distance could specifically be measures. There are a lot of measures out there (Cosine distance, Manhattan distance, Delta, Euclidiean, Entorpy) and a lot of features to measure the distance of (most frequent words, word embeddings, contextual word embeddings, complete document term matrix), but these basic idea with the methods we use in Stylo is that documents are found te be similar if the frequencies of the most frequent words are similar. This is thus a more stylistic distance (we are looking at words like `a`, `the`, `and`) than a more substantive distance. The fact that we cannot explain all these distance measures also mean that not all parameters we choose in this final part will be explained. 

We will explore two ways of exploring relationships between texts: by using (hierarchical) clustering and by multidimensional scaling.

## Hierarchical clustering
To goal of hierarchical clustering is to build this tree-like structure where the documents that are most similar (i.e. have the most overlap in the most frequent words) are placed in so-called "branches". These "branches", then, are also clustered, untill all branches are clustered together.

To perform hierarchical clustering in `Stylo`, we first have to run the general `Stylo` method, which contains various kinds of methods to analyze clusters within our data. In my case, I'm using the complete corpus for the analysis (containing both texts by women and men):

```r
corpus <- 'data/books/corpus'
stylo(gui = TRUE,
      corpus.dir = corpus)
```

Again, after you run these lines, a pop-up window will open where you can specify settings and the methods we want to use. Let's go through these screens one by one.

![](/images/screenshot_30.png)


#### Input & Language
Here, we have to specify the format our texts are in (opposed to AntConc, Stylo is able to work with a variety of formats), which is `plain text`. We also have to specify the language of our texts. This should speak for itself.

#### Features
In this tab, we can decide what features (what words) we use to cluster our text. In the default setting, we cluster the files based on the top 100 most frequent words, which are often function words. If you want to 







