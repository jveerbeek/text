# Introduction to text analysis in AntConc and Stylo

This manual will explain how to perform a basic computational text analysis in AntConc and Stylo (R): don’t worry, this tutorial will (hopefully) guide you through every step with the use of helpful screenshots and textual directions. If you have some experience with either of these programs already, then this tutorial will also be helpful as it will likely show you some ins and outs you didn’t know (though you can probably skip a few steps).

## What is AntConc?
AntConc is a tool for concordance analysis and basic corpus search. This manual explains the four main functions of AntConc: ‘Word List’, ‘Concordance’, ‘Collocations’, ‘Clusters’ and ‘Keyword List’. Most of these functions assume a rule-based (‘top-down’) approach.

## What is Stylo?
Stylo is a package for stylometric analysis developed for R, a programming language and environment for statistical computing. Stylo was created by Maciej Eder, Jan Rybicki and Mike Kestemont.
