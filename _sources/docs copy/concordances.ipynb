{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0a7ad9a8",
   "metadata": {},
   "source": [
    "(getting_started)=\n",
    "\n",
    "\n",
    "# Concordance\n",
    "As discussed in the previous chapter, the default tab of *AntConc*, and the most important one when you are mainly interested in corpus exploration, is the `Concordance` tab. The term ‘concordance’ refers to ‘an alphabetical index of the principal words in a book or the works of an author with their immediate contexts’ (Merriam Webster). A concordance is essentially a list of the search term in its direct context (KWIC, or keyword in context). \n",
    "\n",
    "As a matter of fact, one of the first \"digital humanities\" projects was actually centered around creating a concordance to the work of Thomas Aquinas. This project, led by Roberto Busa, started in 1946, and eventually took 34 years, 10.000 computing hours, 1 million work hours and over 1500 kilometres of wire to complete. One of the reasons computational textual analysis has experiences something of a boom *might* very well be related to the fact that those kind of resources aren't really anymore...\n",
    "\n",
    "To get started with the `Concordance` tab, first make sure your texts are imported! (See previous chapter) Then enter the word you are interested in (I'm going for `men`) in the input field at the bottom on the screen (below `Search term`), and click on `Start`:\n",
    "\n",
    "![](/images/screenshot_5.png)\n",
    "\n",
    "Now, we are simply presented with all matches of the word `men` in our collection of texts. To see a match of a particular text in more detail, you can click on the blue colored words to see the full textual context of the match:\n",
    "\n",
    "![](/images/screenshot_6.png)\n",
    "\n",
    "And to go back to the `Concordance`, you can either click on the black marked text or click on the `Concordance` tab in the above panel. \n",
    "\n",
    "## The parameters\n",
    "Besides our inputting our query, we can adjust various parameters to change how AntConc searches our collection of texts and sorts our results.\n",
    "\n",
    "---\n",
    "\n",
    "First, next to `Search term`, we see three checkboxes. \n",
    "\n",
    "![](/images/screenshot_7.png)\n",
    "\n",
    "We can use the `Words` checkbox to explicitly search for *words*. If you turn this parameters off, and press `Start` again, we will match on a character-level instead, and all words starting with the characters `m`, `e`, and `n` are returned.\n",
    "\n",
    "The `Case` checkbox specifies whether we search in case insensitive (both `men` and `Men` are returned), on in case sensitive mode (only `men` would be returned if we search for `men`). \n",
    "\n",
    "We have to turn the `regex` checkbox on when we want to include [regular expressions](https://en.wikipedia.org/wiki/Regular_expression) in our query. We will discuss these in more detail later.\n",
    "\n",
    "---\n",
    "\n",
    "The `Search window size` parameter allows us to choose the number of characters that are outputted on either side of the search term. If we increase this, more context will be shown, if we decrease this, less context will be shown.\n",
    "\n",
    "![](/images/screenshot_8.png)\n",
    "\n",
    "---\n",
    "\n",
    "`Show Every Nth Row` allows us to limit the number of matches that are returned. If you set this to $1$, all matches will be returned. If you set this number to $2$, only half of the matches will be shown (only every second row). If your query results in a lot of matches, this can help you to explore your results more easily. \n",
    "\n",
    "![](/images/screenshot_9.png)\n",
    "\n",
    "The (somewhat) confusing checkboxes under `Kwic sort` allows us to specify how our matches are sorted. In this case, the matches are first alphabetically sorted by `1R`, which means the *first* (1) word on the right (R) of the target word (in our case `men`), then by the second word on the right, and finally sorted by the third word on the right. If you change, for example, the level 1 parameter to `1L` (by clicking on the down button twice), and press `Sort`, you will see that your results are now sorted by the first (1) word on the left (L): \n",
    "\n",
    "![](/images/screenshot_10.png)\n",
    "\n",
    "Changing how your results are sorted can help you to easily look for certain patterns in your dataset: what words to frequently appear *before* `men`, and what words do frequently appear *after* men?\n",
    "\n",
    "## Using wildcards\n",
    "In the above examples, we only searched for occurences of the word `men`. But what if we want to include other variations, or other words even, like `man` or `women`? We could enter these as seperate query, but we can also choose to use *wildcards*. Wildcards allow us to specify variations of our query. The wildcards that are (by default) included in AntConc are:\n",
    "\n",
    "\n",
    "| Wildcard | Explanation             | Example query | Matched words                          |\n",
    "|----------|-------------------------|---------------|----------------------------------------|\n",
    "| *        | Zero or more characters | men*          | men, ment, menache                     |\n",
    "| +        | Zero or one character   | men+          | men, ment, mena                        |\n",
    "| ?        | Exactly one character   | m?n          | men, man                             |\n",
    "| \\|       | OR                      | men\\|women    | men, women                             |\n",
    "| @        | Zero or one word        | men@@women    | 'men and women', 'men that like woman' |\n",
    "| #        | Exactly one word        | man##woman    | 'man that like women'                  |\n",
    "\n",
    "To experience the power of using wildcards, it's best to try out different (combinations of) wildcards to see what they do!\n",
    "\n",
    "Instead of wildcards, you can also use regular expressions. Regular expressions work very similar to wildcards (and are also used to find different variations of a certain word), but have a little more flexibility -- almost every pattern could be expressed using regular expressions (sometimes also resulting in very long regular expressions). Because learning how to write regular expressions would require its own manual, we will not discuss this in the manual, but there are plently of resources avalaible on the internet to learn about regular expressions.\n",
    "\n",
    "## Concordance plots\n",
    "Besides looking at the textual context of our query, we can also explore the occurences of our query more visually using so-called *concordance plots*. To do this, go to the `Concordance plots` panel. If you see a blank screen, make sure your query is entered below `Search term` and click `Start`. If everything, went right, you will see this:\n",
    "\n",
    "![](/images/screenshot_11.png)\n",
    "\n",
    "The interpretation of these plots is relatively intuitive. The plots show, per text in your corpus, the (relative) position of the occurences  of your query in the text. A vertical line on the left means `men` could be found in the beginning of the text, and a line on the right means it could be found at the end of the text.\n",
    "\n",
    "Though they might look relatively fancy, these plots are (in most cases) completely useless, unless you *specifically* would like to know where in the text certain keywords are located. But then still, because these plots are organized on a document-level (and not on a corpus-level), the actual use-cases for these plots are very limited, in my experience.. \n",
    "\n",
    "If you would actually like to explore the occurences of your keywords in a more quantitative way than just looking at the keywords in context, you are way better of with using clusters/collocations, which we will discuss in the next chapter."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "text_representation": {
    "extension": ".md",
    "format_name": "myst"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  },
  "source_map": [
   10
  ]
 },
 "nbformat": 4,
 "nbformat_minor": 5
}