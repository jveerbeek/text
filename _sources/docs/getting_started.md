---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

(getting_started)=


# Getting started with AntConc
Imagine you have a set of 1000 novels available in digital format. How would you make sense of this material? You can, of course, start by reading the first file from start to finish, then read the second file, then read the third, and so on... But even if you managed to read one novel a day, going through the complete corpus would take you more than two years to complete.

Now you might say, I'm not really interested in reading these novels from start to finish, but only in the representation of a specific theme. You might be interested in the way *climate change* is represented in novels, the extreme right, or animals, or something completely different. So what would you do then? You could open every file separately, use `CTRL+F` to look at the occurrences of the word *climate*, gather them in a separate Word document, and go on the carry out your analysis from there. But then you still wouldn't be able to (quantitatively) map patterns in the way climate change is discussed in these novels.  

But you can also AntConc! AntConc is a simple tool that works *really* well for two things. First, it can just help you to explore your corpus more easily. In this sense, you don't use AntConc as a very sophisticated distant reading tool, but just as a fancy `CTRL+F` tool. Instead of opening every file separately, and looking for the occurrence of a certain word within this file, AntConc allows you to search for the occurrence of a certain word in a *collection* of documents. For example, you can use AntConc to map the occurrence of the word `animal` in a collection of movie reviews:   

![](/images/screenshot_1.png)

Showing you, in one glance, not only the documents this word appears in (listed in `File`), but also the *context* of these occurrences (`KWIC`, or "keyword in context"). We also refer to these contexts as *concordances*. 

Now AntConc is not the only *concordance* tool out there... But besides showing the *concordances* of a certain word, AntConc is also really helpful for performing various kinds of *keyword analyses*. Here, the focus is not so much on the manual *exploration* of your corpus, but on finding patterns and trends within your data. What words do appear frequently in the context of `animal`? What words, in general, characterize my collection of texts? How does this one collection of texts (novels written by females) differ from another collection of texts (novels written by males)? And so on, and so forth...

## Getting your data ready
Part of the simplicity of AntConc is that it only accepts **one** format your data needs to be in before you are able to import it into AntConc: a plain `.txt` file, or a directory of plain `.txt` files. So when you want to compare texts written by males to texts written by females using AntConc, you would have to have one directory containing all the texts written by males, and one directory containing all the texts written by females:

![](/images/screenshot_2.png)

Unfortunately, this manual cannot teach you how to get your data ready in this format, as this is highly dependent on how your dataset is structured in the first place, and we simply cannot discuss all possible options. If your dataset is structured in a `csv` file, for example, you would need to do different things than when your dataset contains multiple files in `pdf` format. In general, however, there are plenty of websites available that can help you convert your files into a plain text format (just Google "convert [your file type] to txt")

## Import your corpus
To import your files, go to `File` (in the menu bar), and click on `Open dir...` (if you want to import a directory of text files) or on `Open file(s)...` (if you want to import just one file). Then navigate to the directory your texts are in and click on `Choose`. If everything went right, you will now see the files in your corpus on the left side of the screen.

![](/images/screenshot_3.png)

In the left bottom corner, you will see how many texts you have imported. In this case, I have imported 200 texts.

## The AntConc interface
So now we know that the left side of the AntConc interface shows your corpus files, how do we make sense of the rest of AntConc's interface? Let's break the general interface down to four different panels:

![](/images/screenshot_4.png)

You already know the first one. The second panel shows the different types of *analyses*/methods for corpus exploration you can carry out in AntConc (AntConc refers to these tabs as *Tools*). The default (inspection) method is the concordance viewer, where you can look for keywords and inspect their context. Next to the concordance viewer are the `Concordance plots` and the `File viewer`, which are both alternative methods for looking at concordances. Other types of analyses listed here are `Clusters/n-gram`, `Collocates`, `Word list` and `Keyword List`, which we will discuss in the next sections. 

The third panel, then, shows the results for the analysis you chose in the second panel. And to carry/start out the actual analysis, you have to press on `Start` in the fourth panel. Additionally, you can define your query here (what word are you searching for), change the parameters of the method, and export the results shown in the third panel (`Clone results`).

Now we know the basics of the AntConc interface, let's get ourselves somewhat more familiar with concordance analysis in AntConc.



