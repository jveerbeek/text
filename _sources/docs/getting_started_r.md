---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Getting started with Stylo/R

With AntConc, we did everything we needed to do using a Graphical User Interface (GUI). Having a GUI to do our analyses is great (though whether AntConc's GUI is great too is up for debate), as we can easily explore different options in the programs and so on. 

But in some cases, you might want a little more flexibility. You might want to adjust your corpus to some extent, exclude certain texts, etc. Having more flexibility is one of the biggest reasons scholars working on computational analyses eventually resort to using programming languages. Moreover, sharing certain methods and analyses with programming scripts is much easier, as you don't have to take care of actually building a GUI, which can be a quite bothersome task. 

Now if the idea of using programming languages scares you, don't worry. We will mainly focus on using Stylo, which is a package written for R (and which also contains a GUI to some degree) and is quite intuitive to use. This manual will also really not teach you the ins and outs of using R. We will not discuss for-loops, functions, and all the things you associate learning to program with.

As said, Stylo is a package for *stylometric* analysis. *Stylometry* is often used to get to know the author of text where the author is unknown, such as the *Wilhelmus*, or books written under a pseudonym, like Marek van der Jagt, who was discovered to be Arnon Grunberg using stylometric methods. This field has developed much of its own methods to find that out, many of them based on the most frequent words of a text (the, a, I), which are actually really speaking for an author's fingerprint. 

This manual, however, also will not teach you how to actually carry out a stylometric analysis. I will mainly discuss the functions and options in `Stylo` that have broader applicability, similar to AntConc. Stylo contains a lot of options, and many of them will, therefore, not be explained in this manual. If you want to learn more about Stylo and stylometric analysis, however, the GitHub of [Stylo](https://github.com/computationalstylistics/stylo) would be a great place to start.

## What is RStudio?
Most users will use R in combination with the program RStudio (https://www.rstudio.com). RStudio can be regarded as a wrapper around R, actively assisting you with many housekeeping tasks. (For users familiar with SPSS, this is somewhat similar to the SPSS graphical user interface wrapped around the SPSS “engine”). After opening, RStudio displays three or four panels or panes, with multiple tabs (in bold) within each pane.

```{note} 
Parts of the introduction to R are taken from Hugo Quene's ["Introduction to R"](https://hugoquene.github.io/emlar2020/ch-introduction.html#sec:whatisrstudio) 
```

![](/images/screenshot_26.png)

The left panel, or lower left panel, has a tab named `Console` which importantly contains the current R session. You can input R commands there (try typing ```date()``` and press `Enter`) and see the output (warning and error messages are displayed in red).

In the top right panel, the tab `Environment` lists all objects in the workspace (see explanation below), and the tab `History` lists you previously entered R commands.

In the bottom right panel, the tab `Files` shows files in your current folder or directory, `Plots` contains plots produced by R/RStudio, and `Help` gives you access to help information.

You could work your way through most of this manual using only the `Console` tab of RStudio, but most users find R+RStudio far easier to work with than R by itself.

Instead, the easiest way to work with RStudio, in my opinion, is to open a script and write your commands in there. To do that, go to `File`, `New file`, `R script`. You will now see a fourth panel:

![](/images/screenshot_27.png)

First, save your script by going to `File` > `Save as..` (or `CTRL+S`),  navigate to the directory your corpus files are located in and save your R-script. 

Finally, let us discuss how you can execute your script. There are roughly three ways do to this: line by line (click `run` or `ctrl` + `enter`), a section (highlight selection and click `run` or `ctrl + enter`, or sourcing the entire script (click `source` or `crtl + shift + s`). It is important to note that when you source, by default you will not see the code in your console. In order to do see the code, you can instead select ‘source with echo’ from the drop-down menu (ctrl + shift + enter).


## Setting the working directory
In most cases, the thing we want to do is setting our working directory right. The working directory is the file path on your computer that sets the default location of the files you read into R. Instead of typing `Users/jorisveerbeek/documents/UU/project/text_analysis/data/` each time you want to retrieve something from the `data` folder, setting the working directory to `text_analysis` allows you to only have to specify `data`. There are two ways to set your working directory: using the interface and using code. 

For the first option, go to `Session` (in the menu bar), `Set working directory`. Here you can either choose `To source location` (which is the folder your script is located in) or `Choose directory`, and navigate to the directory you want to work in. 

After you selected this, you will see a command has been added in the console tab:

```r
setwd("~/text_analysis")
```

This also is the second way to set the working directory: using the `setwd` command. 

If you don't know how to specify the path on a certain file, you can retrieve it using Windows Explorer/Finder. In Windows, open the explorer, press `Shift` and right-click on the folder you want to retrieve the path of, and then click on `Copy as path`. On Mac, open the Finder, navigate to your directory, right-click on the file you want to get the path of, press `Alt`, and click on `Copy [folder] as a pathname`.



## Installing Stylo
To install Stylo, run:

```r
install.packages("stylo")
```

The installer should now be running. If everything went right, the console should look similar to: 

![](/images/screenshot_28.png)

Now we are ready to import the package into R (this is something different than installing the package). To import the package, add the following command to your script and run it:

```r
library(stylo)
```

Now it could be the case that after you run this command you get an error saying that `XQuartz` is missing on your computer. In that case, navigate to the URL listed in the error, download the software and install it on your computer. After that, **restart your computer** (I'm sorry), open Rstudio again, and import stylo again. If everything went right, you should see the following message (and only the following message!):


```r
### stylo version: 0.7.4 ###

If you plan to cite this software (please do!), use the following reference:
    Eder, M., Rybicki, J. and Kestemont, M. (2016). Stylometry with R:
    a package for computational text analysis. R Journal 8(1): 107-121.
    <https://journal.r-project.org/archive/2016/RJ-2016-007/index.html>

To get full BibTeX entry, type: citation("stylo")
```

Now we are ready to explore using Stylo. We will focus on two main functions of Stylo: `oppose` and the more general-purpose method `stylo`. 