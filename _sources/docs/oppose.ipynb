{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ac936537",
   "metadata": {},
   "source": [
    "# Oppose\n",
    "\n",
    "The first function we are going to use is `Oppose`. `Oppose` is Stylo's version of distinctive words (see the chapter distinctive words in AntConc). In short, it is a \"function that performs a contrastive analysis between two given sets of texts\". \n",
    "\n",
    "To calculate the distinctive words, `Stylo`, by default, doesn't use what AntConc calls `Keyness`, but the so-called `Zeta`-measure. The `Zeta` measures is better suited for long texts, such as novels, or transcripts of episodes. It divides the texts into chunks (small samples) of size $N$, and does not compare the *frequency* of the word in these chunks, but whether a word *occurs* in these chunks of text (binarization). Because of this it's actually quite robust to words appearing rather often: [\"Zeta appeals to the Digital Literary Studies community because it is mathematically simple and has a built-in preference for highly interpretable content words. Indeed, Zeta has been successfully applied to various issues in literary history.\"](https://dh2018.adho.org/burrows-zeta-exploring-and-evaluating-variants-and-parameters/)\n",
    "\n",
    "```{admonition} What is the Zeta measure?\n",
    "\"Zeta is calculated by comparing two groups of texts ($G1$ and $G2$). From each text in each group, a sample of $n$ segments of fixed size with $m$ word tokens is taken. For each term ($t$) in the vocabulary (e.g., consisting of lemmatized words), the segment proportions ($sp$) in each group are calculated, i.e. the proportion of segments in which this term appears at least once (binarization). Zeta of $t$ results from subtracting the two segment proportions.\"\n",
    "```\n",
    "\n",
    "To get the most distinctive words, we are going to compare two sets of texts, one by male authors, one by female authors, that are -- just like with our AntConc tutorial -- in two folders, saved in plain text. First we, tell R were our directories containing in target corpus and the reference corpus can be found:\n",
    "\n",
    "```r\n",
    "corpus.male <- 'data/books/male' # change this to your directory containing the texts\n",
    "corpus.female <- 'data/books/female'\n",
    "```\n",
    "\n",
    "Here, we define two variables using `<-`. We do this mainly so that we don't have to write out the full directory every time we want to use. **Don't forget to change the above to your own path**.\n",
    "\n",
    "Then, we can run the `Oppose` function using:\n",
    "\n",
    "```r\n",
    "oppose(gui=TRUE, # whether to use the GUI for adjusting the parameters\n",
    "      primary.corpus.dir = corpus.female,\n",
    "       secondary.corpus.dir = corpus.male\n",
    "                  )\n",
    "\n",
    "```\n",
    "\n",
    "After you ran it, you will presented with this screen:\n",
    "\n",
    "![](/images/screenshot_29.png)\n",
    "\n",
    "In this screen, you can adjust the settings for running Zeta on your texts, and change the output of the analysis. \n",
    "\n",
    "Unless your texts are rather short (then it's good to change to `Slice length` parameters, where you can specify the size of the chunks), it is advised to start out with the default settings. For an emperical evaluation of the different parameters (and different methods you can select under `METHOD`), see [this paper](https://dh2018.adho.org/burrows-zeta-exploring-and-evaluating-variants-and-parameters/)). In most cases, the default settings and using Craig's Zeta will produce good results. \n",
    "\n",
    "\n",
    "```{admonition} The settings of Oppose\n",
    "`Slice length`: the size of the samples/chunks of texts <br>\n",
    "`Slice Overlap`: the overlap between samples. If set 5 five, the second chunk will also consists of the last five words of the first sample, and so on.<br>\n",
    "`Occurence Threshold`: filter out all the words with an occurence less than $N$. <br>\n",
    "`Filter threshold`: the threshold for the `Zeta`-value, between 0 and 1. The higher, the larger the difference has got to be in order to be \"significant\".<br>\n",
    "```\n",
    "\n",
    "\n",
    "Because `Stylo` does not only analyze our texts, but also produces a nice visualization for us, we can also specify the settings for the visualization. In our case, we do not only want the output to be shown in Rstudio, but also immediately save the output as an image, so be sure to click on both the `PDF` and the `PNG` checkbox. All other parameters can be left as is (you can specify, for example, whether you want to plot the words, or only the `Zeta` values). Now click on `OK` to run the analysis. \n",
    "\n",
    "As running the `Zeta` method could be quite computationally intensive, it could take a while for the method is done.. You can check the progress in the `Console` tab. After the method is finished, a plot should appear in the `Plots` tab of Rstudio. Besides that, five files should have been added to your working directory: the plots in PDF, the plots in PNG, `oppose_config.txt`, `word_avoided.txt` and `word_preffered`. First, let's look at our vizualisation:\n",
    "\n",
    "![](/images/text_analysis_final_Craig_001.png)\n",
    "\n",
    "The words above the horizontal line are the words that are most the *preferred* by the authors in our target corpus, i.e. occur  significantly more in text by women compared to text by men. The  words below the horizontal line are the words that are the most *avoided* by the authors in our target corpus, i.e. occur significantly more often in texts by men compared to text by women. \n",
    "\n",
    "These words also be found in the files `words-avoided.txt` and `word-preferred`, which show all the words (without the `Zeta` values) that are *avoided* or *preffered* by our target group of authors. For example, the `words-preferred.txt` looks like:  \n",
    "\n",
    "```\n",
    "# The file contains words that were extracted in Burrows' Zeta test:\n",
    "# this subset lists words significantly AVOIDED by primary author(s).\n",
    "# The list can be used as an input wordlist for other methods, and for this\n",
    "# purpose it can be manually revised, edited, deleted, culled, etc.\n",
    "# You can either delete unwanted words, or mark them with \"#\"\n",
    "# -----------------------------------------------------------------------\n",
    "\n",
    "kind\n",
    "moeder\n",
    "liefde\n",
    "dochter\n",
    "hart\n",
    "zodra\n",
    "water\n",
    "moeders\n",
    "bed\n",
    "slaap\n",
    "nacht\n",
    "ouders\n",
    "meisje\n",
    "armen\n",
    "voeten\n",
    "geur\n",
    "broer\n",
    "vader\n",
    "baby\n",
    "buik\n",
    "kinderen\n",
    "zee\n",
    "haren\n",
    "wind\n",
    "zeven\n",
    "huwelijk\n",
    "slapen\n",
    "and\n",
    "aarde\n",
    "```\n",
    "\n",
    "```{warning}\n",
    "When you run this analysis twice, the files of your previous analysis will be overwritten (without any warning!). When you want to keep your results, be sure to drag them to a different folder. \n",
    "```\n",
    "\n",
    "Finally, the `oppose-config.txt` shows the settings you used for running the analyze, so that you can communicate this settings when you present the results."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "text_representation": {
    "extension": ".md",
    "format_name": "myst"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  },
  "source_map": [
   10
  ]
 },
 "nbformat": 4,
 "nbformat_minor": 5
}