{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "fa540252",
   "metadata": {},
   "source": [
    "(getting_started)=\n",
    "\n",
    "\n",
    "# Clusters and collocates\n",
    "In the previous chapter, we mainly explored the *concordances* of a certain word. When sorting the results by `1L` (see previous chapter), you might already seen some patterns in your data. For example, you might have seen that `men` seemed to occur frequently after words like `brave`, or `white`, `black`, or `young`... But these were mainly *qualitative* impressions; we didn't actually count the co-occurences of the other words, we just sorted them using a certain heuristic and manually inspected the output. \n",
    "\n",
    "When you want to inspect the co-occurences of a certain word (what words do frequently appear in the context of `men`) in a more quantitative way in *AntConc*, you can use two tabs: `Clusters/N-gram` and `Collocates`, both of which look at co-occurences (but in a slightly diffent manner).\n",
    "\n",
    "## Clusters/N-gram\n",
    "The most simple way to analyze co-occurences of a certain word, is by just counting them. `Brave` occurs $5$ times before `men`, `white` $10$ times, `black` $3$ times, and so on and so forth...\n",
    "\n",
    "That's essentially what is done in the `Clusters/N-gram` tab. The term ‘N-gram’ refers to a sequence of words where the number of words in the sequence equals $N$ ($N$ is any number: 2-grams/bigrams: the same two words one after the other; 3-grams/trigrams: the same three words in the same word order). N-grams allow you    to look for recurring phrases or combination of certain words. \n",
    "\n",
    "---\n",
    "\n",
    "Let's actually look at some co-occurences! Instead of `men`, we analyzed last chapter, I will choose to focus on the co-occurences of `man` (singular) instead. To do this, go to the `Clusters/N-gram` tab, make sure to enter `man` (not `men`!) in the `Search term` field, select `On right` under `Search Term Position` (to make sure we look for words occuring *before* men -- not *after*).\n",
    "\n",
    "Now click on `Start`, and the most words that most frequently occur before `man` are shown:\n",
    "\n",
    "![](/images/screenshot_13.png)\n",
    "\n",
    "As you can see here, the most frequently occuring word before `man` is `a`, then followed by `the`. After that, more interesting words start to appear, like `young`, `old`, `black` and `dead`. Now, if we would compare these co-occurences with the most frequent words before `woman`, you might already be able to find some differences between the way men and women are described in texts:\n",
    "\n",
    "![](/images/screenshot_14.png)\n",
    "\n",
    "Whereas none of the most frequent words before `man` refer to appearance, three of the ten most frequent words (`pretty`, `beautiful`, `sexy`) do! \n",
    "\n",
    "## The parameters of clusters/N-gram\n",
    "\n",
    "Now you also might have seen that the panel where we specified our queries has slightly changed compared to the `Concordance` tab. Some options, like using `Regex` for our search query, or `Start` and `Stop` are still there, but there are some new settings we can adjust as well.\n",
    "\n",
    "Now one of the most important ones is `Cluster` size. Here, we specify the mimumum and the maximum number of words appearing before `men` we take into account. If you set the `Max` to three, you will also see clusters of three words (for example: `grumpy old man`) appearing in the results. If you set the `Min` also to three, only clusters with more than three words will be shown.\n",
    "\n",
    "We can also specify `Min freq` and `Min range`. This is quite similar to the `Show every Nth row` we discussed in the `Concordance` tab, as it also limits the number of results we will get. Setting `Min freq` allows us to specify a minimum frequency of a certain cluster. If we set this to $2$, for example, only clusters that appear more than two times will show up. `Min. Range` allows us the specify the minimum number of *documents* the cluster should appear in. It could be these case that a certain phrase (such `Men in Black`) only appears very frequently in one document. Setting the `Min. Range` to two would get rid of such clusters .\n",
    "Furthermore, we can specify the `Search Term Position`. In the previous step, we set this to `On right` to get all the words appearing *before* `men`. To get all the words appearing *after* `men`, change this parameter to `On left`. Again, just try it out to see what happens!\n",
    "\n",
    "Finally, after `Search Term` you will see that besides `Words`, `Case`, and `Regex` another options has been added: `N-grams`. If you turn this option on and press `Start` you will not get the most frequent occuring words before/after your query, but the most frequent N-grams (where $N$ is related to the parameters you specify in `N-gram size`) in *general*. Running this method could actually take a while (you change make it go slightly faster by increasing the `Min Freq.` parameters. For example, here are the most frequent N-grams with `Min.` set to $3$ and `Max.` to $4$:\n",
    "\n",
    "![](/images/screenshot_15.png)\n",
    "\n",
    "## Collocates\n",
    "\n",
    "To get analyze the co-occurences of the word `man`, we started this chapter with the most simple method: counting the most frequent words left to the word ‘man’. Here, we saw that the two most frequent words were function words, and weren’t really telling.\n",
    "\n",
    "Okay, `a` appears the most frequently in the context of `man`, but `a` occurs frequently in the context of almost every (singular) noun. Try, for example, to get the most frequent words before `horse`, or before `lie`. Here, the most frequent words appearing before these words are probably also `a`. \n",
    "\n",
    "Now, we could use a list of stop words to manually filter out all these (not really informative) function words. We throw ‘the’ out of the window, and ‘a’, and ‘those’. But what if these words actually would be meaningful?\n",
    "\n",
    "Well, instead of using a list of stop words, we can also use a more *statistical* measure.. Here, we try to find the words that co-occur more often with ‘men’ than what expected, given what we know about the frequency of these co-occurences with other words. This is what a *collocation* is: ‘a combination of words or terms that co-occur in a certain window more often than would be expected by chance’. Collocations can be useful to trace semantic associations between words in a given text or corpus. \n",
    "\n",
    "To get the collocations of a `man`, move over to the `Collocates` tab, make sure `man` is entered in the `Search Term` input field, and press `Start`. Now you will get the most significant collocates of the word `man`:\n",
    "\n",
    "![](/images/screenshot_16.png)\n",
    "\n",
    "Unless you would have expected beforehand th at `whoring` to be an important collocate for `man`, the results might strike you as not really intuitive. `Whoring`, `whitworth`, `whitehouse`, `wed`... why does AntConc returns us these rather obscure W-words? \n",
    "\n",
    "Well that is because to calculate what words are actually collocates, AntConc is internally using a measure called Mutual Information (MI), which heavily favoures very infrequent words. You can see this for yourself by looking at the `Freq` column: almost all the words listed above have a frequency of $1$. To combat this, it is commonly recommended to increase the `Min Collocate Frequency` to a value of $5$ or above. Once we do that, the results will start to make a lot more sense:\n",
    "\n",
    "![](/images/screenshot_17.png)\n",
    "\n",
    "And for `woman`:\n",
    "\n",
    "![](/images/screenshot_18.png)\n",
    "\n",
    "Now, not `the` and `a` aren't shown in the top of the results anymore, but words that actually reveal some semantic associations with the word `woman`, like `beautiful`, `pretty` and `young`.   \n",
    "\n",
    "## The settings of collocates\n",
    "As with any other tab, some new options have been added to the menu below the results -- in this case: two new options. \n",
    "\n",
    "The first is the `Window Span`, were you specify span of words to the left and to the right of the query in which to find collocates. Note that unlike in the `clusters` tab, where only words *immediately* to the left and right of certain word are counted, you can also chose to count `beautiful blonde man` as a co-occurence of `man` and `beautiful`. Even more so: `5L` and `5R` (five words to the right, five to the left) is actually the default setting. The effect of this could be seen in the previous screenshot, where `she` is listed in the top collocates of `women`, even though `she` almost never appears directly next to women. You can also see this when you click on `she` in the results. If you click on the option `Same`, the `From` will be made equal to `To..` when you change it.\n",
    "\n",
    "The second is the `Min collocate frequency`. We already used this one to filter out all the strange collocates that had a frequency of $1$. Now $5$ is generally a recommended value, but you can try to change this setting to see what is does."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "text_representation": {
    "extension": ".md",
    "format_name": "myst"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  },
  "source_map": [
   10
  ]
 },
 "nbformat": 4,
 "nbformat_minor": 5
}